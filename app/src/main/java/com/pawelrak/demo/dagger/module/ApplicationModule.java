package com.pawelrak.demo.dagger.module;

import android.content.Context;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.pawelrak.demo.NavigationBus;
import com.pawelrak.demo.navigation.Navigator;
import com.pawelrak.demo.navigation.handler.ActivityForResultNavigationHandler;
import com.pawelrak.demo.navigation.handler.ActivityNavigationHandler;
import com.pawelrak.demo.navigation.handler.NavigationHandler;

import java.util.Arrays;
import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.pawelrak.data.network.Api.apiURL;

/**
 * Created by pawelrak on 14.03.2017.
 */

@Singleton
@Module
public class ApplicationModule {

    private Context context;

    public ApplicationModule(Context context) {
        this.context = context;
    }

    @Provides
    Context provideContext() {
        return context;
    }

    @Provides
    GoogleApiClient provideGoogleApiClient(GoogleSignInOptions googleSignInOptions) {
        return new GoogleApiClient.Builder(context)
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();
    }

    @Provides
    GoogleSignInOptions provideGoogleSignInOptions() {
        return new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
    }

    @Singleton
    @Provides
    Navigator provideNavigator(NavigationBus navigationBus, List<NavigationHandler> navigationHandlers) {
        return new Navigator(navigationBus, navigationHandlers);
    }

    @Singleton
    @Provides
    List<NavigationHandler> provideNavigationHandlers() {
        return Arrays.asList(
                new ActivityNavigationHandler(),
                new ActivityForResultNavigationHandler()
        );
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder().baseUrl(apiURL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        return builder.build();
    }


}
