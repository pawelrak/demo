package com.pawelrak.demo.dagger.component;

import android.content.Context;

import com.google.android.gms.common.api.GoogleApiClient;
import com.pawelrak.data.network.Network;
import com.pawelrak.data.util.Preferences;
import com.pawelrak.demo.NavigationBus;
import com.pawelrak.demo.dagger.module.ApplicationModule;
import com.pawelrak.demo.navigation.Navigator;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by pawelrak on 14.03.2017.
 */

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {
    Context context();

    NavigationBus rxBus();

    GoogleApiClient googleApiClient();

    Navigator navigator();

    Preferences preferences();

    Network network();

    class Initializer {
        private Initializer() {
//            no instance
        }

        public static ApplicationComponent init(Context context) {
            return DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(context))
                    .build();
        }
    }
}
