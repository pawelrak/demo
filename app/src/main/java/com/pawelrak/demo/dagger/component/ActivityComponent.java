package com.pawelrak.demo.dagger.component;

import com.pawelrak.data.dagger.ActivityScope;
import com.pawelrak.demo.dagger.module.ActivityModule;
import com.pawelrak.demo.view.MainActivity;
import com.pawelrak.demo.view.UserDetailsActivity;

import dagger.Component;

/**
 * Created by pawelrak on 14.03.2017.
 */

@ActivityScope
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class})
public interface ActivityComponent {

    void inject(MainActivity mainActivity);

    void inject(UserDetailsActivity userDetailsActivity);

    class Initializer {
        private Initializer() {
        }

        public static ActivityComponent init(ApplicationComponent applicationComponent) {
            return DaggerActivityComponent.builder()
                    .applicationComponent(applicationComponent)
                    .activityModule(new ActivityModule())
                    .build();
        }
    }
}
