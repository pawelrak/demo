package com.pawelrak.demo;

import com.pawelrak.demo.navigation.event.NavigationEvent;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;


@Singleton
public class NavigationBus {

    private final Subject<NavigationEvent, NavigationEvent> bus = new SerializedSubject<>(PublishSubject.create());

    @Inject
    public NavigationBus() {

    }

    public void send(NavigationEvent event) {
        bus.onNext(event);
    }

    public Observable<NavigationEvent> toObservable() {
        return bus;
    }

    public boolean hasObservers() {
        return bus.hasObservers();
    }
}

