package com.pawelrak.demo.navigation.handler;

import android.app.Activity;

import com.pawelrak.demo.navigation.event.ActivityForResultNavigationEvent;
import com.pawelrak.demo.navigation.event.NavigationEvent;

import static com.pawelrak.demo.view.MainActivity.RC_SIGN_IN;

public class ActivityForResultNavigationHandler implements NavigationHandler<Activity, ActivityForResultNavigationEvent> {

    @Override
    public boolean canHandle(Activity activity, NavigationEvent navigationEvent) {
        return navigationEvent instanceof ActivityForResultNavigationEvent;
    }

    @Override
    public void handle(Activity activity, ActivityForResultNavigationEvent event) {

        if (event.getIntent() != null)
            activity.startActivityForResult(event.getIntent(), RC_SIGN_IN);

    }
}