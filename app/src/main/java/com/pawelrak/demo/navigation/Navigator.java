package com.pawelrak.demo.navigation;

import android.app.Activity;

import com.pawelrak.demo.NavigationBus;
import com.pawelrak.demo.navigation.event.NavigationEvent;
import com.pawelrak.demo.navigation.handler.NavigationHandler;

import java.util.List;

public class Navigator {

    private final NavigationBus navigationBus;
    private final List<NavigationHandler> navigationHandlers;
    private Activity currentActivity;

    public Navigator(NavigationBus navigationBus, List<NavigationHandler> navigationHandlers) {
        this.navigationBus = navigationBus;
        this.navigationHandlers = navigationHandlers;
        listen();
    }

    private void listen() {
        navigationBus.toObservable().subscribe(this::handleEvent);
    }

    private void handleEvent(NavigationEvent event) {
        for (NavigationHandler navigationHandler : navigationHandlers) {
            if (navigationHandler.canHandle(currentActivity, event)) {
                navigationHandler.handle(currentActivity, event);
                break;
            }
        }
    }

    public Activity getCurrentActivity() {
        return currentActivity;
    }

    public void setCurrentActivity(Activity currentActivity) {
        this.currentActivity = currentActivity;
    }

    public void destroy(Activity activity) {
        if (currentActivity == activity)
            currentActivity = null;
    }
}