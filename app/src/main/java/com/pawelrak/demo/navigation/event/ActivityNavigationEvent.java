package com.pawelrak.demo.navigation.event;

import android.os.Bundle;

public class ActivityNavigationEvent implements NavigationEvent {

    private boolean finishCaller;
    private Class activityToStart;
    private Bundle bundle;

    public ActivityNavigationEvent(boolean finishCaller, Class activityToStart) {
        this.finishCaller = finishCaller;
        this.activityToStart = activityToStart;
    }

    public ActivityNavigationEvent(boolean finishCaller, Class activityToStart, Bundle bundle) {
        this.finishCaller = finishCaller;
        this.activityToStart = activityToStart;
        this.bundle = bundle;
    }

    public Class getActivityToStart() {
        return activityToStart;
    }

    public boolean shouldFinishCaller() {
        return finishCaller;
    }

    public Bundle getBundle() {
        return bundle;
    }
}