package com.pawelrak.demo.navigation.event;

import android.content.Intent;

public class ActivityForResultNavigationEvent implements NavigationEvent {

    private Intent intent;

    public ActivityForResultNavigationEvent(Intent intent, int resultCode) {
        this.intent = intent;
    }

    public Intent getIntent() {
        return intent;
    }
}