package com.pawelrak.demo.navigation.handler;

import android.app.Activity;
import android.content.Intent;

import com.pawelrak.demo.navigation.event.ActivityNavigationEvent;
import com.pawelrak.demo.navigation.event.NavigationEvent;

public class ActivityNavigationHandler implements NavigationHandler<Activity, ActivityNavigationEvent> {

    @Override
    public boolean canHandle(Activity activity, NavigationEvent navigationEvent) {
        return navigationEvent instanceof ActivityNavigationEvent;
    }

    @Override
    public void handle(Activity activity, ActivityNavigationEvent event) {
        Intent intent = new Intent(activity, event.getActivityToStart());

        // add extras if needed
        if (event.getBundle() != null)
            intent.putExtras(event.getBundle());

        activity.startActivity(intent);

        // kill old activity if commanded
        if (event.shouldFinishCaller())
            activity.finish();
    }
}