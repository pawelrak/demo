package com.pawelrak.demo.navigation.handler;

import android.app.Activity;

import com.pawelrak.demo.navigation.event.NavigationEvent;


public interface NavigationHandler<A extends Activity, T extends NavigationEvent> {

    boolean canHandle(Activity activity, NavigationEvent navigationEvent);

    void handle(A activity, T event);
}
