package com.pawelrak.demo.viewmodel;

import android.databinding.ObservableField;
import android.text.TextUtils;
import android.view.View;

import com.pawelrak.data.dagger.ActivityScope;
import com.pawelrak.data.interactor.ExtendedSubscriber;
import com.pawelrak.data.interactor.GoogleDataInteractor;
import com.pawelrak.data.model.User;
import com.pawelrak.data.model.network.UpdateUserResponse;
import com.pawelrak.data.util.Preferences;
import com.pawelrak.demo.NavigationBus;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by pawelrak on 14.03.2017.
 */

@ActivityScope
public class UserDetailsViewModel extends BaseViewModel {

    public ObservableField<String> googleId;
    private Preferences preferences;
    private GoogleDataInteractor googleDataInteractor;
    private User user;

    @Inject
    public UserDetailsViewModel(NavigationBus navigationBus, Preferences preferences, GoogleDataInteractor googleDataInteractor) {
        super(navigationBus);
        this.preferences = preferences;
        this.googleId = new ObservableField<>(preferences.getGoogleId());
        this.googleDataInteractor = googleDataInteractor;

        user = new User();
    }

    public View.OnClickListener getOnSendDataClick() {
        return view -> {
            updateUser();
        };
    }

    private void updateUser() {
        if (!TextUtils.isEmpty(googleId.get())) {
            user.setGoogleId(googleId.get());
            googleDataInteractor.setUser(user);
            googleDataInteractor.execute(new UpdateUserSubscriber());
        }
    }

    public void unsubscribe() {
        googleDataInteractor.unsubscribe();
    }

    private class UpdateUserSubscriber extends ExtendedSubscriber<UpdateUserResponse> {

        UpdateUserSubscriber() {
        }

        @Override
        public void onSubscribe() {
        }

        @Override
        public void onTerminate() {
        }

        @Override
        public void onServerError(String error) {
        }

        @Override
        public void onServerError(int error) {
        }

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
        }

        @Override
        public void onNext(UpdateUserResponse updateUserResponse) {
            Timber.d("Response comes here.");
        }

    }

}
