package com.pawelrak.demo.viewmodel;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.pawelrak.data.dagger.ActivityScope;
import com.pawelrak.data.util.Preferences;
import com.pawelrak.demo.NavigationBus;
import com.pawelrak.demo.navigation.event.ActivityForResultNavigationEvent;
import com.pawelrak.demo.navigation.event.ActivityNavigationEvent;
import com.pawelrak.demo.view.UserDetailsActivity;

import javax.inject.Inject;

import static com.pawelrak.demo.view.MainActivity.RC_SIGN_IN;

/**
 * Created by pawelrak on 14.03.2017.
 */

@ActivityScope
public class MainViewModel extends BaseViewModel {

    private GoogleApiClient googleApiClient;
    private Preferences preferences;

    @Inject
    public MainViewModel(NavigationBus navigationBus, GoogleApiClient googleApiClient, Preferences preferences) {
        super(navigationBus);
        this.googleApiClient = googleApiClient;
        this.preferences = preferences;
    }

    public GoogleApiClient getGoogleApiClient() {
        return googleApiClient;
    }

    public void proceedToUserDetailsActivity() {
        navigationBus.send(new ActivityNavigationEvent(false, UserDetailsActivity.class));
    }

    public View.OnClickListener getOnGoogleSignIn() {
        return view -> {
            signIn();
        };
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(getGoogleApiClient());
        navigationBus.send(new ActivityForResultNavigationEvent(signInIntent, RC_SIGN_IN));
    }

    public void handleGoogleApiData(GoogleSignInAccount googleSignInAccount) {
        String googleId = googleSignInAccount.getId();
        if (!TextUtils.isEmpty(googleId)) {
            preferences.saveGoogleId(googleId);
        }
    }

    public void handleActivityResult(int requestCode, Intent data) {
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                handleGoogleApiData(result.getSignInAccount());
            }
            proceedToUserDetailsActivity();
        }
    }
}
