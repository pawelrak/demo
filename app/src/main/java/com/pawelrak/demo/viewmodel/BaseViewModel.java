package com.pawelrak.demo.viewmodel;

import android.databinding.BaseObservable;

import com.pawelrak.demo.NavigationBus;

public abstract class BaseViewModel extends BaseObservable {

    protected NavigationBus navigationBus;

    public BaseViewModel(NavigationBus navigationBus) {
        this.navigationBus = navigationBus;
    }
}