package com.pawelrak.demo.view;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.pawelrak.demo.R;
import com.pawelrak.demo.databinding.ActivityMainBinding;
import com.pawelrak.demo.viewmodel.MainViewModel;

import javax.inject.Inject;


public class MainActivity extends BaseActivity {

    public static int RC_SIGN_IN = 100;

    @Inject
    MainViewModel viewModel;

    ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setViewModel(viewModel);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        viewModel.handleActivityResult(requestCode, data);
    }

    @Override
    protected void injectMembers() {
        getActivityComponent().inject(this);
    }
}
