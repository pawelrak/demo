package com.pawelrak.demo.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.pawelrak.demo.R;
import com.pawelrak.demo.databinding.ActivityUserDetailsBinding;
import com.pawelrak.demo.viewmodel.UserDetailsViewModel;

import javax.inject.Inject;

public class UserDetailsActivity extends BaseActivity {

    @Inject
    UserDetailsViewModel viewModel;

    ActivityUserDetailsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_details);
        binding.setViewModel(viewModel);
    }

    @Override
    protected void injectMembers() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModel.unsubscribe();
    }
}
