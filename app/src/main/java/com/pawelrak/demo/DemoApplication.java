package com.pawelrak.demo;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import com.pawelrak.demo.dagger.component.ApplicationComponent;
import com.pawelrak.demo.navigation.Navigator;

import timber.log.Timber;

/**
 * Created by pawelrak on 14.03.2017.
 */

public class DemoApplication extends Application implements Application.ActivityLifecycleCallbacks {

    private Navigator navigator;
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        this.initializeInjector();
        initNavigator();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

    }

    private void initNavigator() {
        navigator = applicationComponent.navigator();
        registerActivityLifecycleCallbacks(this);
    }

    public void initializeInjector() {
        this.applicationComponent = ApplicationComponent.Initializer.init(this);
    }

    public ApplicationComponent getApplicationComponent() {
        if (applicationComponent == null)
            initializeInjector();
        return this.applicationComponent;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        navigator.setCurrentActivity(activity);
    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {
        Activity navigatorActivity = navigator.getCurrentActivity();
        if (navigatorActivity == null || navigatorActivity != activity)
            navigator.setCurrentActivity(activity);
    }

    @Override
    public void onActivityPaused(Activity activity) {
    }

    @Override
    public void onActivityStopped(Activity activity) {
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        navigator.destroy(activity);
    }
}
