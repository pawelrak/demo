package com.pawelrak.data.network;

import com.pawelrak.data.model.User;
import com.pawelrak.data.model.network.UpdateUserResponse;

import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by pawelrak on 16.03.2017.
 */

public interface Api {

    public static String apiURL = "http://someapi.com/index.php/";

    @POST(apiURL + "users/{userId}")
    Observable<UpdateUserResponse> updateUser(@Path("userId") String userId, @Body User updatedUser);
}
