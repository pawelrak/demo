package com.pawelrak.data.network;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Retrofit;

/**
 * Created by pawelrak on 16.03.2017.
 */

@Singleton
public class Network {

    private Retrofit retrofit;
    private Api api;

    @Inject
    public Network(Retrofit retrofit) {
        this.retrofit = retrofit;
        buildApi();
    }

    private void buildApi() {
        api = retrofit.create(Api.class);
    }

    public Api getApi() {
        return api;
    }

}
