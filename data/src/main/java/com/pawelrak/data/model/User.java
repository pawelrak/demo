package com.pawelrak.data.model;

/**
 * Created by pawelrak on 14.03.2017.
 */

public class User {
    String googleId;

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }
}
