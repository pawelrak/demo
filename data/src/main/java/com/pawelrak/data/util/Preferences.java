package com.pawelrak.data.util;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by pawelrak on 14.03.2017.
 */

@Singleton
public class Preferences {

    public static final String PREFS = "prefs";
    public static final String GOOGLE_ID = "googleId";

    protected SharedPreferences sharedPreferences;


    @Inject
    public Preferences(Context context) {
        sharedPreferences = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
    }

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    public void saveGoogleId(String googleId) {
        sharedPreferences.edit().putString(GOOGLE_ID, googleId).apply();
    }

    public String getGoogleId() {
        return sharedPreferences.getString(GOOGLE_ID, "No ID passed.");
    }

    public void clearPrefs() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }
}
