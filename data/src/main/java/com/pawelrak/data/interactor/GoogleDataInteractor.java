package com.pawelrak.data.interactor;

import com.pawelrak.data.dagger.ActivityScope;
import com.pawelrak.data.model.User;
import com.pawelrak.data.network.Network;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by pawelrak on 16.03.2017.
 *
 * Mockup class for updating user on the server (using RxJava)
 */

@ActivityScope
public class GoogleDataInteractor extends BaseInteractor {

    private User user;
    private Network network;

    private String mockUserId = "123";

    @Inject
    public GoogleDataInteractor(Network network) {
        this.network = network;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    protected Observable buildInteractorObservable(ExtendedSubscriber extendedSubscriber) {
        return network.getApi().updateUser(mockUserId, user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(extendedSubscriber::onSubscribe)
                .doOnTerminate(extendedSubscriber::onTerminate);
    }


}
