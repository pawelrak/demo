package com.pawelrak.data.interactor;

import rx.Subscriber;

public abstract class ExtendedSubscriber<T> extends Subscriber<T> {

    public abstract void onSubscribe();

    public abstract void onTerminate();

    public void onServerError(int error) {
    }

    public abstract void onServerError(String error);

    @Override
    public void onError(Throwable e) {
    }

}