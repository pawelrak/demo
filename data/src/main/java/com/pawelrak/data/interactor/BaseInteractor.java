package com.pawelrak.data.interactor;

import rx.Observable;
import rx.Subscription;
import rx.subscriptions.Subscriptions;

public abstract class BaseInteractor {

    private Subscription subscription = Subscriptions.empty();

    protected abstract Observable buildInteractorObservable(ExtendedSubscriber extendedSubscriber);

    @SuppressWarnings("unchecked")
    public void execute(ExtendedSubscriber extendedSubscriber) {
        subscription = buildInteractorObservable(extendedSubscriber).subscribe(extendedSubscriber);
    }

    public void unsubscribe() {
        if(!subscription.isUnsubscribed())
            subscription.unsubscribe();
    }
}